package com.sandbox.laba4

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class FillMatrixTest {
    @Test
    fun testMatrixOfSizeException() {
        assertThrows<IllegalArgumentException> { MatrixBuilder.build(0) }
    }

    @Test
    fun `size 1`() {
        val testMatrix: SquareMatrix = MatrixBuilder.build(1)
        val correctMatrix = SquareMatrix(1)

        correctMatrix.setCell(0, 0, 1)

        testMatrix.setCell(0, 0, FillMatrix.getCell(0, 0, 1))

        if (testMatrix == correctMatrix){
            println("Matrix is correct")
        }
        else{
            println("Matrix isn't correct")
        }
    }

    @Test
    fun `size 2`() {
        val testMatrix: SquareMatrix = MatrixBuilder.build(2)
        val correctMatrix = SquareMatrix(2)

        correctMatrix.setCell(0, 0, 4)
        correctMatrix.setCell(1, 0, 3)
        correctMatrix.setCell(0, 1, 1)
        correctMatrix.setCell(1, 1, 2)

        testMatrix.setCell(0, 0, FillMatrix.getCell(0, 0, 2))
        testMatrix.setCell(1, 0, FillMatrix.getCell(1, 0, 2))
        testMatrix.setCell(0, 1, FillMatrix.getCell(0, 1, 2))
        testMatrix.setCell(1, 1, FillMatrix.getCell(1, 1, 2))

        if (testMatrix == correctMatrix){
            println("Matrix is correct")
        }
        else{
            println("Matrix isn't correct")
        }
    }

    @Test
    fun `size 3`() {
        val testMatrix: SquareMatrix = MatrixBuilder.build(3)
        val correctMatrix = SquareMatrix(3)

        correctMatrix.setCell(0, 0, 7)
        correctMatrix.setCell(1, 0, 6)
        correctMatrix.setCell(2, 0, 5)
        correctMatrix.setCell(0, 1, 8)
        correctMatrix.setCell(1, 1, 9)
        correctMatrix.setCell(2, 1, 4)
        correctMatrix.setCell(0, 2, 1)
        correctMatrix.setCell(1, 2, 2)
        correctMatrix.setCell(2, 2, 3)

        testMatrix.setCell(0, 0, FillMatrix.getCell(0, 0, 3))
        testMatrix.setCell(1, 0, FillMatrix.getCell(1, 0, 3))
        testMatrix.setCell(2, 0, FillMatrix.getCell(2, 0, 3))
        testMatrix.setCell(0, 1, FillMatrix.getCell(0, 1, 3))
        testMatrix.setCell(1, 1, FillMatrix.getCell(1, 1, 3))
        testMatrix.setCell(2, 1, FillMatrix.getCell(2, 1, 3))
        testMatrix.setCell(0, 2, FillMatrix.getCell(0, 2, 3))
        testMatrix.setCell(1, 2, FillMatrix.getCell(1, 2, 3))
        testMatrix.setCell(2, 2, FillMatrix.getCell(2, 2, 3))

        if (testMatrix == correctMatrix){
            println("Matrix is correct")
        }
        else{
            println("Matrix isn't correct")
        }
    }

    @Test
    fun `size 4`() {
        val testMatrix: SquareMatrix = MatrixBuilder.build(4)
        val correctMatrix = SquareMatrix(4)

        correctMatrix.setCell(0, 0, 10)
        correctMatrix.setCell(1, 0, 9)
        correctMatrix.setCell(2, 0, 8)
        correctMatrix.setCell(3, 0, 7)
        correctMatrix.setCell(0, 1, 11)
        correctMatrix.setCell(1, 1, 14)
        correctMatrix.setCell(2, 1, 15)
        correctMatrix.setCell(3, 1, 6)
        correctMatrix.setCell(0, 2, 12)
        correctMatrix.setCell(1, 2, 13)
        correctMatrix.setCell(2, 2, 16)
        correctMatrix.setCell(3, 2, 5)
        correctMatrix.setCell(0, 3, 1)
        correctMatrix.setCell(1, 3, 2)
        correctMatrix.setCell(2, 3, 3)
        correctMatrix.setCell(3, 3, 4)

        testMatrix.setCell(0, 0, FillMatrix.getCell(0, 0, 4))
        testMatrix.setCell(1, 0, FillMatrix.getCell(1, 0, 4))
        testMatrix.setCell(2, 0, FillMatrix.getCell(2, 0, 4))
        testMatrix.setCell(3, 0, FillMatrix.getCell(3, 0, 4))
        testMatrix.setCell(0, 1, FillMatrix.getCell(0, 1, 4))
        testMatrix.setCell(1, 1, FillMatrix.getCell(1, 1, 4))
        testMatrix.setCell(2, 1, FillMatrix.getCell(2, 1, 4))
        testMatrix.setCell(3, 1, FillMatrix.getCell(3, 1, 4))
        testMatrix.setCell(0, 2, FillMatrix.getCell(0, 2, 4))
        testMatrix.setCell(1, 2, FillMatrix.getCell(1, 2, 4))
        testMatrix.setCell(2, 2, FillMatrix.getCell(2, 2, 4))
        testMatrix.setCell(3, 2, FillMatrix.getCell(3, 2, 4))
        testMatrix.setCell(0, 3, FillMatrix.getCell(0, 3, 4))
        testMatrix.setCell(1, 3, FillMatrix.getCell(1, 3, 4))
        testMatrix.setCell(2, 3, FillMatrix.getCell(2, 3, 4))
        testMatrix.setCell(3, 3, FillMatrix.getCell(3, 3, 4))


        if (testMatrix == correctMatrix){
            println("Matrix is correct")
        }
        else{
            println("Matrix isn't correct")
        }
    }
}
