package com.sandbox.laba4

/**git
 * Класс квадратной матрицы.
 * @param _size размер матрицы. Корректны целые значчения больше нуля.
 * Конструктор принимает размер матрицы и создает массив соотвествующего размера.
 */

class SquareMatrix(_size: Int){
    private var size: Int = _size
    private var array: Array<Int> = Array(_size * _size) {0}
    fun getSize(): Int { return size } // геттер размера матрицы, для получения размера матрицы
    fun getCell(x: Int, y: Int): Int { return array[(y * size) + x] } // геттер ячейки, для получения ячейки

    fun setCell(x: Int, y: Int, value: Int) { array[(y * size) + x] = value } // сеттер ячейки, для задания ячейки

    /**git
     * переопределение оператора сравнения для типа SquareMatrix.
     */
    override fun equals(other: Any?): Boolean{
        if (other is SquareMatrix){
            if (this.getSize() == other.getSize()){

                var x: Int
                var y = 0
                while(y < this.getSize()){

                    x = 0
                    while(x < this.getSize()){

                        if (this.getCell(x, y) != other.getCell(x, y)){ return false }
                        x++
                    }
                    y++
                }
                return true
            }
            else{
                return false
            }
        }
        else{
            return false
        }
    }
}
