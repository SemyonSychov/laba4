package com.sandbox.laba4

/**git
 * Класс билдер матрицы. Создает матрицу получаемого размера.
 */

object MatrixBuilder { // builder квадратной матрицы
    /**git
     * Метод создающий матрицу.
     * @param mtrxSize размер матрицы. Корректны целые значчения больше нуля.
     * @return SquareMatrix мустая матрица введенного размера.
     */
    fun build(mtrxSize: Int): SquareMatrix { // Builder квадратной матици
        if (mtrxSize < 1) {
            throw IllegalArgumentException()
        }
        return SquareMatrix(mtrxSize)
    }
}
